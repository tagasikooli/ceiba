#!/bin/bash

# Connect
printf "Connecting to machine.. "
eval $(docker-machine env tagasi-kooli-01)
echo "DONE"

# Check active machine
printf "Active machine is.. "
docker-machine active
read -p "Deploy to this machine? (y/N)" -n 1 -r
echo    # (optional) move to a new line
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi

printf "Deploying to server "
docker-machine ip tagasi-kooli-01
docker-compose -f docker-compose.yml up -d --build && docker-compose logs -f --tail=100
