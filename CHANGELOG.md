## 2019-04-19

- Slot view now show status.
- Don't show developer tested rows
- Unhide passed slots in /slot

## 2019-04-18

- Email server moved to Mailgun to increase deliverability

## 2019-03-26

- Notification email to office about bookings

## 2019-03-13

- Fix: Don't show slots with passed registration due dates
- Fix: Make Swedbank logo more visible