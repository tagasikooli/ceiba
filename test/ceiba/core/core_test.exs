defmodule Ceiba.CoreTest do
  use Ceiba.DataCase

  alias Ceiba.Core

  describe "slots" do
    alias Ceiba.Core.Slot

    @valid_attrs %{
      createdBy: "some createdBy",
      datetime: "2010-04-17T14:00:00Z",
      description: "some description",
      filler: "some filler",
      info: %{},
      location: "some location",
      title: "some title",
      type: "some type"
    }
    @update_attrs %{
      createdBy: "some updated createdBy",
      datetime: "2011-05-18T15:01:01Z",
      description: "some updated description",
      filler: "some updated filler",
      info: %{},
      location: "some updated location",
      title: "some updated title",
      type: "some updated type"
    }
    @invalid_attrs %{
      createdBy: nil,
      datetime: nil,
      description: nil,
      filler: nil,
      info: nil,
      location: nil,
      title: nil,
      type: nil
    }

    def slot_fixture(attrs \\ %{}) do
      {:ok, slot} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Core.create_slot()

      slot
    end

    test "list_slots/0 returns all slots" do
      slot = slot_fixture()
      assert Core.list_slots() == [slot]
    end

    test "get_slot!/1 returns the slot with given id" do
      slot = slot_fixture()
      assert Core.get_slot!(slot.id) == slot
    end

    test "create_slot/1 with valid data creates a slot" do
      assert {:ok, %Slot{} = slot} = Core.create_slot(@valid_attrs)
      assert slot.createdBy == "some createdBy"
      assert slot.datetime == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert slot.description == "some description"
      assert slot.filler == "some filler"
      assert slot.info == %{}
      assert slot.location == "some location"
      assert slot.title == "some title"
      assert slot.type == "some type"
    end

    test "create_slot/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Core.create_slot(@invalid_attrs)
    end

    test "update_slot/2 with valid data updates the slot" do
      slot = slot_fixture()
      assert {:ok, %Slot{} = slot} = Core.update_slot(slot, @update_attrs)
      assert slot.createdBy == "some updated createdBy"
      assert slot.datetime == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert slot.description == "some updated description"
      assert slot.filler == "some updated filler"
      assert slot.info == %{}
      assert slot.location == "some updated location"
      assert slot.title == "some updated title"
      assert slot.type == "some updated type"
    end

    test "update_slot/2 with invalid data returns error changeset" do
      slot = slot_fixture()
      assert {:error, %Ecto.Changeset{}} = Core.update_slot(slot, @invalid_attrs)
      assert slot == Core.get_slot!(slot.id)
    end

    test "delete_slot/1 deletes the slot" do
      slot = slot_fixture()
      assert {:ok, %Slot{}} = Core.delete_slot(slot)
      assert_raise Ecto.NoResultsError, fn -> Core.get_slot!(slot.id) end
    end

    test "change_slot/1 returns a slot changeset" do
      slot = slot_fixture()
      assert %Ecto.Changeset{} = Core.change_slot(slot)
    end
  end

  describe "slots_registration" do
    alias Ceiba.Core.SlotRegistration

    @valid_attrs %{created_at: "2010-04-17T14:00:00Z", info: %{}, size: 42}
    @update_attrs %{created_at: "2011-05-18T15:01:01Z", info: %{}, size: 43}
    @invalid_attrs %{created_at: nil, info: nil, size: nil}

    def slot_registration_fixture(attrs \\ %{}) do
      {:ok, slot_registration} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Core.create_slot_registration()

      slot_registration
    end

    test "list_slots_registration/0 returns all slots_registration" do
      slot_registration = slot_registration_fixture()
      assert Core.list_slots_registration() == [slot_registration]
    end

    test "get_slot_registration!/1 returns the slot_registration with given id" do
      slot_registration = slot_registration_fixture()
      assert Core.get_slot_registration!(slot_registration.id) == slot_registration
    end

    test "create_slot_registration/1 with valid data creates a slot_registration" do
      assert {:ok, %SlotRegistration{} = slot_registration} =
               Core.create_slot_registration(@valid_attrs)

      assert slot_registration.created_at ==
               DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")

      assert slot_registration.info == %{}
      assert slot_registration.size == 42
    end

    test "create_slot_registration/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Core.create_slot_registration(@invalid_attrs)
    end

    test "update_slot_registration/2 with valid data updates the slot_registration" do
      slot_registration = slot_registration_fixture()

      assert {:ok, %SlotRegistration{} = slot_registration} =
               Core.update_slot_registration(slot_registration, @update_attrs)

      assert slot_registration.created_at ==
               DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")

      assert slot_registration.info == %{}
      assert slot_registration.size == 43
    end

    test "update_slot_registration/2 with invalid data returns error changeset" do
      slot_registration = slot_registration_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Core.update_slot_registration(slot_registration, @invalid_attrs)

      assert slot_registration == Core.get_slot_registration!(slot_registration.id)
    end

    test "delete_slot_registration/1 deletes the slot_registration" do
      slot_registration = slot_registration_fixture()
      assert {:ok, %SlotRegistration{}} = Core.delete_slot_registration(slot_registration)

      assert_raise Ecto.NoResultsError, fn ->
        Core.get_slot_registration!(slot_registration.id)
      end
    end

    test "change_slot_registration/1 returns a slot_registration changeset" do
      slot_registration = slot_registration_fixture()
      assert %Ecto.Changeset{} = Core.change_slot_registration(slot_registration)
    end
  end

  describe "feedback" do
    alias Ceiba.Core.Feedback

    @valid_attrs %{info: %{}}
    @update_attrs %{info: %{}}
    @invalid_attrs %{info: nil}

    def feedback_fixture(attrs \\ %{}) do
      {:ok, feedback} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Core.create_feedback()

      feedback
    end

    test "list_feedback/0 returns all feedback" do
      feedback = feedback_fixture()
      assert Core.list_feedback() == [feedback]
    end

    test "get_feedback!/1 returns the feedback with given id" do
      feedback = feedback_fixture()
      assert Core.get_feedback!(feedback.id) == feedback
    end

    test "create_feedback/1 with valid data creates a feedback" do
      assert {:ok, %Feedback{} = feedback} = Core.create_feedback(@valid_attrs)
      assert feedback.info == %{}
    end

    test "create_feedback/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Core.create_feedback(@invalid_attrs)
    end

    test "update_feedback/2 with valid data updates the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{} = feedback} = Core.update_feedback(feedback, @update_attrs)
      assert feedback.info == %{}
    end

    test "update_feedback/2 with invalid data returns error changeset" do
      feedback = feedback_fixture()
      assert {:error, %Ecto.Changeset{}} = Core.update_feedback(feedback, @invalid_attrs)
      assert feedback == Core.get_feedback!(feedback.id)
    end

    test "delete_feedback/1 deletes the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{}} = Core.delete_feedback(feedback)
      assert_raise Ecto.NoResultsError, fn -> Core.get_feedback!(feedback.id) end
    end

    test "change_feedback/1 returns a feedback changeset" do
      feedback = feedback_fixture()
      assert %Ecto.Changeset{} = Core.change_feedback(feedback)
    end
  end
end
