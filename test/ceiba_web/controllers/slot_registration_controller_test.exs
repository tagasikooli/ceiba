defmodule CeibaWeb.SlotRegistrationControllerTest do
  use CeibaWeb.ConnCase

  alias Ceiba.Core

  @create_attrs %{created_at: "2010-04-17T14:00:00Z", info: %{}, size: 42}
  @update_attrs %{created_at: "2011-05-18T15:01:01Z", info: %{}, size: 43}
  @invalid_attrs %{created_at: nil, info: nil, size: nil}

  def fixture(:slot_registration) do
    {:ok, slot_registration} = Core.create_slot_registration(@create_attrs)
    slot_registration
  end

  describe "index" do
    test "lists all slots_registration", %{conn: conn} do
      conn = get(conn, Routes.slot_registration_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Slots registration"
    end
  end

  describe "new slot_registration" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.slot_registration_path(conn, :new))
      assert html_response(conn, 200) =~ "New Slot registration"
    end
  end

  describe "create slot_registration" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn =
        post(conn, Routes.slot_registration_path(conn, :create), slot_registration: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.slot_registration_path(conn, :show, id)

      conn = get(conn, Routes.slot_registration_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Slot registration"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        post(conn, Routes.slot_registration_path(conn, :create), slot_registration: @invalid_attrs)

      assert html_response(conn, 200) =~ "New Slot registration"
    end
  end

  describe "edit slot_registration" do
    setup [:create_slot_registration]

    test "renders form for editing chosen slot_registration", %{
      conn: conn,
      slot_registration: slot_registration
    } do
      conn = get(conn, Routes.slot_registration_path(conn, :edit, slot_registration))
      assert html_response(conn, 200) =~ "Edit Slot registration"
    end
  end

  describe "update slot_registration" do
    setup [:create_slot_registration]

    test "redirects when data is valid", %{conn: conn, slot_registration: slot_registration} do
      conn =
        put(conn, Routes.slot_registration_path(conn, :update, slot_registration),
          slot_registration: @update_attrs
        )

      assert redirected_to(conn) == Routes.slot_registration_path(conn, :show, slot_registration)

      conn = get(conn, Routes.slot_registration_path(conn, :show, slot_registration))
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{
      conn: conn,
      slot_registration: slot_registration
    } do
      conn =
        put(conn, Routes.slot_registration_path(conn, :update, slot_registration),
          slot_registration: @invalid_attrs
        )

      assert html_response(conn, 200) =~ "Edit Slot registration"
    end
  end

  describe "delete slot_registration" do
    setup [:create_slot_registration]

    test "deletes chosen slot_registration", %{conn: conn, slot_registration: slot_registration} do
      conn = delete(conn, Routes.slot_registration_path(conn, :delete, slot_registration))
      assert redirected_to(conn) == Routes.slot_registration_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.slot_registration_path(conn, :show, slot_registration))
      end
    end
  end

  defp create_slot_registration(_) do
    slot_registration = fixture(:slot_registration)
    {:ok, slot_registration: slot_registration}
  end
end
