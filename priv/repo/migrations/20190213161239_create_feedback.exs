defmodule Ceiba.Repo.Migrations.CreateFeedback do
  use Ecto.Migration

  def change do
    create table(:feedback) do
      add :info, :map
      add :role, :string
      add :slot_registration_id, references(:slots_registration), null: false

      timestamps()
    end
  end
end
