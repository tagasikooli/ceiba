defmodule Ceiba.Repo.Migrations.CreateSlotsRegistration do
  use Ecto.Migration

  def change do
    create table(:slots_registration) do
      add :info, :map
      add :slot_id, references(:slots), null: false

      timestamps()
    end
  end
end
