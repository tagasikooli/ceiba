defmodule Ceiba.Repo.Migrations.CreateSlots do
  use Ecto.Migration

  def change do
    create table(:slots) do
      add :info, :map
      add :email_sent_booking_closed, :boolean

      timestamps()
    end
  end
end
