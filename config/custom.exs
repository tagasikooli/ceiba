use Mix.Config

config :ceiba, Custom,
  event_types: %{
    excursion: %{
      slot_info_fields: [
        %{
          key: :kuupaev,
          label: "1. Kuupäev",
          label_short: "Kuupäev",
          type: :date
        },
        %{
          key: :ajavahemik,
          label: "2. Ajavahemik",
          label_short: "Ajavahemik",
          type: :string
        },
        %{
          key: :organisatsioon,
          label:
            "3. Organisatsiooni nimi, kui õppekäik toimub mõnes allasutustes, siis organisatsiooni ja allasutuse nimi",
          label_short: "Organisatsioon",
          type: :string
        },
        %{
          key: :piirkond,
          label: "4. Piirkond",
          label_short: "Piirkond",
          type: :string
        },
        %{
          key: :aadress,
          label: "5. Aadress, kus toimub õppekäik",
          label_short: "Aadress",
          type: :string
        },
        %{
          key: :klass,
          label: "6. Klass(id) või klasside vahemik kellele õppekäik on suunatud",
          label_short: "Klassid",
          type: :string
        },
        %{
          key: :maks_opilasi,
          label: "7. Maksimaalne õpilaste arv, mida saate ühel õppekäigul vastu võtta",
          label_short: "Maksimaalne õpilaste arv",
          type: :integer
        },
        %{
          key: :seotud_oppeained,
          label: "8. Teemad, millega on õppekäik seotud",
          label_short: "Teemad",
          type: :string
        },
        %{
          key: :pealkiri,
          label:
            "9. Õppekäigu pealkiri, võimalusel õpilasekohase küsimusena nt kuidas teha põlevkivist õli?",
          label_short: "Pealkiri",
          type: :string
        },
        %{
          key: :opieesmark,
          label: "10. Üks õpilasest lähtuv õpieesmärk, nt tean kuidas elati aastal 1992 vmt.",
          label_short: "Õpieesmärk",
          type: :string
        },
        %{
          key: :tegevus,
          label:
            "11. Tegevus, mida õpilane õppekäigu jooksul teeb, nt iga õpilane saab vahetada rublasid kroonideks vmt.",
          label_short: "Tegevus",
          type: :string
        },
        %{
          key: :iseloomustavad_sonad,
          label:
            "12. Õppekäiku iseloomustavad märksõnad, nt pangandus, rahatarkus, kodanikuteadlikkus jne.",
          label_short: "Märksõnad",
          type: :string
        },
        %{
          key: :keel,
          label: "13. Läbiviimise keel või keeled, nt eesti, vene, inglise või eesti või vene",
          label_short: "Keel",
          type: :string
        },
        %{
          key: :kommentaar,
          label:
            "14. Kommentaar, nt vahetusjalanõud kaasa või kellele kirjutada toidu ette tellimiseks, link menüüle ja mitu päeva ette tuleb toit ette tellida",
          label_short: "Kommentaar",
          type: :string
        },
        %{
          key: :nimekiri_vajalik,
          label: "15. Kas soovite, et õpetaja lisab  osalejate nimekirja",
          label_short: "Osalejate nimekiri",
          type: :boolean
        },
        %{
          key: :hind,
          label: "16. Giidi või eritegevuste hind",
          label_short: "Giidi või eritegevuste hind",
          type: :integer
        },
        %{
          key: :reg_sulgemine,
          label: "17. Mis on viimane päev broneerimiseks (k.a.)",
          label_short: "Broneerimine lõpeb",
          type: :date
        },
        %{
          key: :kontakt,
          label: "18. Kontaktisiku ees- ja perekonnanimi",
          label_short: "Kontaktisiku nimi",
          type: :string
        },
        %{
          key: :email,
          label: "19. Kontaktisiku meiliaadress",
          label_short: "Kontaktisiku e-post",
          type: :string
        },
        %{
          key: :labiviija,
          label: "20. Läbiviija ees- ja perekonnanimi",
          label_short: "Läbiviija nimi",
          type: :string
        }
      ],
      registration_info_fields: [
        %{
          key: :opetaja_nimi,
          label: "Õpetaja nimi",
          type: :string
        },
        %{
          key: :email,
          label: "E-post",
          type: :string
        },
        %{
          key: :telefon,
          label: "Telefon",
          type: :integer
        },
        %{
          key: :kool,
          label: "Kool",
          type: :string
        },
        %{
          key: :klass,
          label: "Klass",
          type: :string
        },
        %{
          key: :lisainfo,
          label: "Lisainfo (nt. erivajadustega õplane)",
          type: :string
        },
        %{
          key: :eelteadmised,
          label: "Õpilaste eelteadmine ja ootused õppekäigule",
          type: :string
        },
        %{
          key: :opilaste_arv,
          label: "Õpilaste arv",
          type: :integer
        },
        %{
          key: :osalejate_nimekiri,
          label: "Osalejate nimekiri",
          type: :string
        }
      ]
    }
  },
  estonia_regions: [
    "Tallinn",
    "Tartu",
    "Pärnu",
    "Narva",
    "Valga",
    "Põlva",
    "Võru",
    "Harju maakond"
  ],
  feedback_questions: %{
    "student" => [
      %{
        key: :huvitav,
        label: "Õppekäik oli minu arvates huvitav",
        type: :score5
      },
      %{
        key: :sain_teada,
        label_fn: &"#{&1}",
        type: :score5
      },
      %{
        key: :sobrale,
        label: "Kui tõenäoliselt soovitaksid Sa õppekäiku oma sõbrale?",
        type: :score5agree
      },
      %{
        key: :sona,
        label: "Üks sõna, mis võtab õppekäigu hästi kokku on",
        type: :string
      }
    ],
    "teacher" => [
      %{
        key: :hinnang,
        label: "Kuidas hindate õppekäiku/haridusprogrammi, milles osalesite?",
        type: :score10
      },
      %{
        key: :eesmark,
        label:
          "Palun kirjutage siia ühe lausega õppekäiku/haridusprogrammi õpi-eesmärk nii nagu teie sellest aru saite?",
        type: :string
      },
      %{
        key: :tegevused,
        label:
          "Millised tegevused toetasid teie arvates kõige rohkem või vähem õpieesmärgi saavutamist?",
        type: :string
      },
      %{
        key: :tulevik,
        label:
          "Millised on teemad või esinejad, mida tulevikus võiksime õppekäikudesse/haridustegevusse võtta?",
        type: :string
      },
      %{
        key: :soovitaks,
        label: "Kui tõenäoliselt te soovitaksite seda õppekäiku/haridustegevust oma kolleegile?",
        type: :score10
      }
    ]
  },
  subjects: [
    "Ajalugu, kodanikukasvatus",
    "Eesti keel ja kirjandus, humanitaarteaduste infootsing",
    "Eesti keel ja kirjandus, teatmekirjanduse infootsing",
    "Elukestev õpe, rahatarkus",
    "Elukestev õpe, rahatarkus, säästmine",
    "Karjääri planeerimine, pangandus, karjäärilood",
    "Kunst ja muusika, raamatukogu infootsing",
    "Loodusteadused, taastuvenergia",
    "Tehnoloogia ja innovatsioon, pangandus, digiteadlikkus"
  ]
