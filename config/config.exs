# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ceiba,
  ecto_repos: [Ceiba.Repo]

# Configures the endpoint
config :ceiba, CeibaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4aCMJCuMK2Emp0t92fbzms1XS75at0FlZlrSzzrHPDr5wu77jWhoIFEGa1kD9TXo",
  render_errors: [view: CeibaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ceiba.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Connect our DB to Mix
config :ceiba, ecto_repos: [Ceiba.Repo]

# Custom configuration
import_config "custom.exs"

config :ceiba, Ceiba.Mailer,
  adapter: Bamboo.MailgunAdapter,
  api_key: System.get_env("MAILGUN_APIKEY"),
  domain: System.get_env("MAILGUN_DOMAIN"),
  region: System.get_env("MAILGUN_REGION")

# Basic password authentication
config :ceiba,
  basic: [
    username: {:system, "LOCK_USERNAME"},
    password: {:system, "LOCK_PASSWORD"},
    realm: "admin"
  ]

# Check to send emails
config :ceiba, Ceiba.Scheduler,
  jobs: [
    {{:extended, "*/15 * * * *"}, fn -> Ceiba.Email.slots_passed_registration_date() end},
    {"@daily", fn -> Ceiba.Email.slots_passed_registration_date() end}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
