
## Vaja teha

- [x] Swed logo
- [x] Order search by date
- [x] Õppeaine/Teema otsingusse uued read
- [x] Tagasiside valikule lisada numbrid
- [x] Õppeaine/Teema esimene valik oleks "Kõik õppekäigud"
- [x] Teksti muudatus: Otsingus "Õppeaine" -> "Teema"
- [x] "Lisa uus õppekäik" asendada "Õppeaine" sõnaga "Teema"
- [x] Tagasiside andmise õnnestumisel näidata teksti: "Aitäh Sulle tagasiside eest!"
- [x] Õppekäikude pealkirjad läksid kõik suurte tähtedega, vaja oleks vaid esimene.

## Esimesest arendusest jäi välja

- [ ] "broneeringut ei tehtud" email (kuupäevaga mitte päevades)
- [ ] Toetuse märge kodulehele
- [ ] õpilase nimekirja functionaalsus
- [ ] Emaili domeen SMTP ümber muuta

## Tehtud

- [x] õppekäigu info lühivorm (alt väljad)
- [x] emailis on nad ühel real ja slot vaates ka
- [x] registreerimise väljad muuta
- [x] tekstid asendada docist
- [x] maha tõmmatud ka ära võtta
- [x] Success uus tekst
- [x] Ajavahemik
- [x] Õppeaine search dropdowniks (meilis on list)
- 0/0 email ei tööta productionis
- 0/0 õpetaja meilist parool välja
- 0/0 koondtabelis õpetaja/õpilase inff sassis
- 0/0 Uus serveri deploy
- 0/0 uuenda deploy dokumentatsiooni
- 1/1 kid-hacker võiks mitte saada ligi feedbackile, reg infole https://github.com/cultivatehq/basic_auth
- 0/1 Widget firma kodulehele VÕI link firma kodulehele (lingi teme ikka ära)
- 0/0 firmale oma link kust näeb omi tehtud töid
- 0/0 pealehel oleks pealkiri suure tähega
- 0/0 otsingumootor jääb lehele lahti aga võtame tühikud ära
- 1/1 EMAIL: Õppekäigule regades saaavad meili õps ja org info koopiaga koos tagasiside linkidega
- 0/0 peida ära hidden fields feedbackis
- 1/1 väljade järjekorra selliseks nagu google sheetsil
- 1/0 Teibile/ettevõttele näha kõik toimunud registreerimised koos org nimega ja koond-tagasisidega
- 0/0 palun Tallinn, Tartu, Narva jne ilma linnata:)
- 0/0 kogu regatud tekst hästi kompaktseks - ilma tühikuteta
- 0/0 nupp on broneeri
- 0/0 pealehel broneeri nupp eraldi "lisainfo/broneerimine"
- 1/1 Otsingu filter - õppeaine, kuupäev - vahemik, piirkond
- 0/0 grupi suuruse arvestamine
- 0/0 flow korda (lingid)
- 0/0 otsing korda (default kuupäevad, piirkond)
- 0/0 otsing töötaks kui kirjutad ainult "matem"
- 0/0 sisestamise vorm kompaktsemaks
- 0/0 pealehele info: pealkiri, kuupäev, organisatsioon, piirkond, klass, hind
- 0/0 pealehele pealkirjad tagasi
- 0/0 Tõlkida tekstid õppekäigu keskseks
- 0/0 Suvaline kasutaja ei saaks lõhkuda
- 0/0 Piirkonna väli teha staatiliseks, mitte aadress
- 0/0 väljad kinnitada
- 0/0 Põhi tabel pealeheks
- 0/0 osalejate nimekiri copy-paste
- 0/0 Maakonnad on valitavad
- 1/1 Tagasiside loogika üldse
- 0/1 Tagasiside leht õpetajale. samamoodi 3 küsimust.
- 1/1 Õpilase tagasiside: meeldis 1-5, õppeväljundi 1-5, kommentaar.
- 0/0 Ekskursioon kaob nimekirjast ära kui üks grupp on reganud
- 0/0 Registreerida ei saa kui on üks juba ees.
- 0/1 Jäta mällu täidetud info (ja täida kasutaja eest)
- 0/1 Õppekäigu vaate info süstematiseerida
- 0/1 UI new sloti vaates kasutajasõbralikumaks
