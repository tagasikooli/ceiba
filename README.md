# Tagasikooli New Information System

## Use widget on your website

Just use this piece of HTML

```
<iframe src="https://tagasikooli.ee/widget?organisatsioon=Minu+Firma"/>
```

## Useful admin pages

- List all slots `/slot`

## Launch local dev server

### Beginner tutorial

1. Install Docker ([Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows), [Mac OS](https://hub.docker.com/editions/community/docker-ce-desktop-mac)).
1. Log into docker
1. Install [GitHub Desktop](https://desktop.github.com/).
1. Start GitHub Desktop and find the clone command and insert this link: `https://gitlab.com/tagasikooli/ceiba.git`
1. Open the project in VS Code
1. Open the terminal from the top menu
1. Run command `docker-compose up`.
1. Website is available on [localhost](http://localhost).

### For developers

```
git clone git@gitlab.com:tagasikooli/ceiba.git
cd ceiba
docker-compose up
# Listening on localhost
```

### Run tests

```
docker-compose down                     # empty the database
docker-compose run --rm webapp mix test
```

## Production server

## Deploy production server

Guide taken from [Deploy ReviewNinja](https://www.digitalocean.com/community/tutorials/how-to-self-host-reviewninja-on-digitalocean-using-docker-and-coreos). If you need a cluster use [docker-swarm](https://www.digitalocean.com/community/tutorials/how-to-create-a-cluster-of-docker-containers-with-docker-swarm-and-digitalocean-on-ubuntu-16-04) (or use DigitalOcean Kubernetes).

**Install prerequisites**

- Arch Linux `yaourt -S docker docker-compose docker-machine && systemctl start docker`

**Create database**

1. Our favorite host is compose.io
2. Create a PostgreSQL database called `production`.

**Create server on DigitalOcean**

First go get the [DIGITAL_OCEAN_ACCESS_TOKEN](https://cloud.digitalocean.com/settings/api/tokens).

```
docker-machine create --driver=digitalocean \
--digitalocean-access-token=DIGITALOCEAN_API_TOKEN \
--digitalocean-image=coreos-stable \
--digitalocean-region=fra1 \
--digitalocean-size=s-2vcpu-2gb \
--digitalocean-ssh-user=core \
tagasi-kooli-01
```

**Connect docker-machine to CoreOS server**

`eval $(docker-machine env tagasi-kooli-01)`

**Build and Deploy**

1. Create a `.env` file with values specified in `docker-compose.yml`
2. Run `./deploy.sh`.

**Enable SSL**

This is a manual process

1. SSH into the container with `docker-compose exec nginx /bin/bash`
1. Run certbot `certbot --nginx`. (To update run `certbot renew`)
1. Answer all questions _wisely_.
