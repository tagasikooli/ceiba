defmodule Ceiba.Repo do
  use Ecto.Repo,
    otp_app: :ceiba,
    adapter: Ecto.Adapters.Postgres
end
