defmodule Ceiba.Email do
  use Bamboo.Phoenix, view: CeibaWeb.EmailView
  alias Ceiba.Mailer

  def send(template, email_address, subj, assignments) do
    IO.puts("Send email: #{email_address} #{subj}")

    new_email()
    |> to(email_address)
    |> bcc("tagasikooli-oppekaigud-emailer@krister.ee")
    |> from("info@tagasikooli.ee")
    |> subject(subj)
    |> put_html_layout({CeibaWeb.LayoutView, "email.html"})
    |> render("#{template}.html", assignments)
    |> Mailer.deliver_later()
  end

  def slot_registration(conn, slot_registration) do
    slot = Ceiba.Core.get_slot!(slot_registration.slot_id)
    creator_email = slot.info.email
    filler_email = slot_registration.info.email

    assignments = %{
      conn: conn,
      slot: slot,
      slot_registration: slot_registration
    }

    send("registration_password", "info@tagasikooli.ee", "Õppekäigu broneering", assignments)
    send("registration_password", creator_email, "Õppekäigu broneering", assignments)
    send("registration", filler_email, "Õppekäigu broneering", assignments)
  end

  def slots_passed_registration_date do
    slots = Ceiba.Core.list_slots_passed_booking_date()

    for slot <- slots do
      Ceiba.Core.update_slot(slot, %{email_sent_booking_closed: true})
      send("booking_passed", slot.info.email, "Õppekäiku ei broneeritud", %{slot: slot})
      send("booking_passed", "info@tagasikooli.ee", "Õppekäiku ei broneeritud", %{slot: slot})
    end
  end
end
