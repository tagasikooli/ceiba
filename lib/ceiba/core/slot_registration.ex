defmodule Ceiba.Core.SlotRegistration do
  use Ecto.Schema
  import Ecto.Changeset

  schema "slots_registration" do
    embeds_one :info, Ceiba.Core.SlotRegistrationInfo
    belongs_to :slot, Ceiba.Core.Slot
    has_many :feedback, Ceiba.Core.Feedback

    timestamps()
  end

  @doc false
  def changeset(slot_registration, attrs) do
    slot_registration
    |> cast(attrs, [:slot_id])
    |> cast_embed(:info)
    |> foreign_key_constraint(:slot_id)
    |> validate_required([:slot_id])
  end
end
