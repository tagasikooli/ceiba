defmodule Ceiba.Core.Feedback do
  use Ecto.Schema
  import Ecto.Changeset

  schema "feedback" do
    field :info, :map
    field :role, :string
    belongs_to :slot_registration, Ceiba.Core.SlotRegistration

    timestamps()
  end

  @doc false
  def changeset(feedback, attrs) do
    attrs =
      if Map.has_key?(attrs, "info") and is_bitstring(attrs["info"]) do
        Map.replace!(attrs, "info", Poison.decode!(attrs["info"]))
      else
        attrs
      end

    feedback
    |> cast(attrs, [:info, :slot_registration_id, :role])
    |> validate_required([:info, :slot_registration_id, :role])
  end
end
