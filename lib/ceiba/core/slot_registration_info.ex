defmodule Ceiba.Core.SlotRegistrationInfo do
  use Ecto.Schema
  import Ecto.Changeset

  @dynamic_fields Application.get_env(:ceiba, Custom)[:event_types][:excursion][
                    :registration_info_fields
                  ]
                  |> Map.new(&{&1.key, &1.type})

  embedded_schema do
    for {field_name, type} <- @dynamic_fields do
      field(field_name, type)
    end
  end

  def changeset(info, attrs) do
    info
    |> cast(attrs, Map.keys(@dynamic_fields))
  end
end
