defmodule Ceiba.Core do
  @moduledoc """
  The Core context.
  """

  import Ecto.Query, warn: false
  alias Ceiba.Repo

  alias Ceiba.Core.Slot
  alias Ceiba.Core.SlotRegistration

  @doc """
  Returns the list of slots.

  ## Examples

      iex> list_slots()
      [%Slot{}, ...]

  """
  def list_slots(with_passed \\ false) do
    get_slot_with_reg_count(with_passed)
    |> Repo.all()
  end

  def list_slots(params, with_passed) do
    params
    |> Map.take(["seotud_oppeained", "piirkond", "alg", "lopp", "organisatsioon"])
    |> Enum.reduce(get_slot_with_reg_count(with_passed), fn p, acc ->
      case p do
        {_, ""} ->
          acc

        {"seotud_oppeained", "Kõik õppekäigud"} ->
          acc

        {"alg", _} ->
          {:ok, start_time} =
            NaiveDateTime.new(
              String.to_integer(params["alg"]["year"]),
              String.to_integer(params["alg"]["month"]),
              String.to_integer(params["alg"]["day"]),
              0,
              0,
              0
            )

          from s in acc,
            where: fragment("?->>'kuupaev' >= ?", s.info, ^"#{start_time}")

        {"lopp", _} ->
          {:ok, end_time} =
            NaiveDateTime.new(
              String.to_integer(params["lopp"]["year"]),
              String.to_integer(params["lopp"]["month"]),
              String.to_integer(params["lopp"]["day"]),
              0,
              0,
              0
            )

          from s in acc, where: fragment("?->>'kuupaev' <= ?", s.info, ^"#{end_time}")

        {key, value} ->
          from s in acc, where: fragment("?->>? ILIKE ?", s.info, ^key, ^"%#{value}%")
      end
    end)
    |> Repo.all()
  end

  def list_slots_passed_booking_date do
    query =
      from s in Slot,
        left_join: sr in SlotRegistration,
        on: s.id == sr.slot_id,
        where: is_nil(sr.id),
        where: fragment("CAST(?->>'reg_sulgemine' AS date) < NOW()", s.info),
        where: is_nil(s.email_sent_booking_closed)

    Repo.all(query)
  end

  def list_slots_with_feedback(params) do
    # For every slot
    for slot <- list_slots(params) do
      # Get it's feedback (array)
      get_slot_feedback(slot.id)

      # Group by teacher/student
      |> Enum.group_by(& &1.role)

      # for either group
      |> Map.new(fn {role, fb_arr} ->
        # sum up scores
        Enum.reduce(fb_arr, %{}, fn map, acc ->
          # ...in the info key
          Map.get(map, :info)

          # This is where the summing happens
          |> Map.merge(acc, fn _key, v1, v2 ->
            result1 = Ceiba.Core.Helpers.parse_integer(v1)
            result2 = Ceiba.Core.Helpers.parse_integer(v2)

            case {result1, result2} do
              {{:ok, no1}, {:ok, no2}} -> no1 + no2
              _ -> v1 <> "; " <> v2
            end
          end)
        end)

        # Calculate average for scores
        |> Map.new(fn {question, answer} ->
          if is_bitstring(answer) do
            {question, answer}
          else
            {question, answer / length(fb_arr)}
          end
        end)

        # Feedback should still be a map
        |> (&{role, &1}).()
      end)

      # Feedback should be in Slot
      |> (&Map.put(slot, :feedback, &1)).()
    end
  end

  def get_slot_with_reg_count(with_passed) do
    query =
      from s in Slot,
        left_join: sr in SlotRegistration,
        on: s.id == sr.slot_id,
        group_by: s.id,
        order_by: fragment("?->>'kuupaev'", s.info),
        select_merge: %{count_registrations: count(sr.id)}

    if !with_passed do
      from s in query,
        where: fragment("?->>'reg_sulgemine' >= ?", s.info, ^"#{Date.utc_today()}")
    else
      query
    end
  end

  @doc """
  Gets a single slot.

  Raises `Ecto.NoResultsError` if the Slot does not exist.

  ## Examples

      iex> get_slot!(123)
      %Slot{}

      iex> get_slot!(456)
      ** (Ecto.NoResultsError)

  """
  def get_slot!(id), do: Repo.get!(Slot, id)

  @doc """
  Creates a slot.

  ## Examples

      iex> create_slot(%{field: value})
      {:ok, %Slot{}}

      iex> create_slot(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_slot(attrs \\ %{}) do
    %Slot{}
    |> Slot.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a slot.

  ## Examples

      iex> update_slot(slot, %{field: new_value})
      {:ok, %Slot{}}

      iex> update_slot(slot, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_slot(%Slot{} = slot, attrs) do
    slot
    |> Slot.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Slot.

  ## Examples

      iex> delete_slot(slot)
      {:ok, %Slot{}}

      iex> delete_slot(slot)
      {:error, %Ecto.Changeset{}}

  """
  def delete_slot(%Slot{} = slot) do
    Repo.delete(slot)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking slot changes.

  ## Examples

      iex> change_slot(slot)
      %Ecto.Changeset{source: %Slot{}}

  """
  def change_slot(%Slot{} = slot) do
    Slot.changeset(slot, %{})
  end

  @doc """
  Returns the list of slots_registration.

  ## Examples

      iex> list_slots_registration()
      [%SlotRegistration{}, ...]

  """
  def list_slots_registration do
    Repo.all(SlotRegistration)
  end

  def count_slots_registration(slot_id) do
    from(SlotRegistration)
    |> where(slot_id: ^slot_id)
    |> Repo.aggregate(:count, :id)
  end

  @doc """
  Gets a single slot_registration.

  Raises `Ecto.NoResultsError` if the Slot registration does not exist.

  ## Examples

      iex> get_slot_registration!(123)
      %SlotRegistration{}

      iex> get_slot_registration!(456)
      ** (Ecto.NoResultsError)

  """
  def get_slot_registration!(id), do: Repo.get!(SlotRegistration, id)

  @doc """
    Gets all registrations for slot
  """
  def get_slot_registrations!(slot_id),
    do: SlotRegistration |> where(slot_id: ^slot_id) |> Repo.all()

  @doc """
  Creates a slot_registration.

  ## Examples

      iex> create_slot_registration(%{field: value})
      {:ok, %SlotRegistration{}}

      iex> create_slot_registration(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_slot_registration(attrs \\ %{}) do
    %SlotRegistration{}
    |> SlotRegistration.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a slot_registration.

  ## Examples

      iex> update_slot_registration(slot_registration, %{field: new_value})
      {:ok, %SlotRegistration{}}

      iex> update_slot_registration(slot_registration, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_slot_registration(%SlotRegistration{} = slot_registration, attrs) do
    slot_registration
    |> SlotRegistration.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a SlotRegistration.

  ## Examples

      iex> delete_slot_registration(slot_registration)
      {:ok, %SlotRegistration{}}

      iex> delete_slot_registration(slot_registration)
      {:error, %Ecto.Changeset{}}

  """
  def delete_slot_registration(%SlotRegistration{} = slot_registration) do
    Repo.delete(slot_registration)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking slot_registration changes.

  ## Examples

      iex> change_slot_registration(slot_registration)
      %Ecto.Changeset{source: %SlotRegistration{}}

  """
  def change_slot_registration(%SlotRegistration{} = slot_registration) do
    SlotRegistration.changeset(slot_registration, %{})
  end

  alias Ceiba.Core.Feedback

  @doc """
  Returns the list of feedback.

  ## Examples

      iex> list_feedback()
      [%Feedback{}, ...]

  """
  def list_feedback do
    Repo.all(Feedback)
  end

  def get_slot_feedback(slot_id) do
    query =
      from slot in Slot,
        inner_join: reg in SlotRegistration,
        on: slot.id == reg.slot_id,
        inner_join: fb in Feedback,
        on: reg.id == fb.slot_registration_id,
        where: slot.id == ^slot_id,
        select: fb

    Repo.all(query)
  end

  @doc """
  Gets a single feedback.

  Raises `Ecto.NoResultsError` if the Feedback does not exist.

  ## Examples

      iex> get_feedback!(123)
      %Feedback{}

      iex> get_feedback!(456)
      ** (Ecto.NoResultsError)

  """
  def get_feedback!(id), do: Repo.get!(Feedback, id)

  @doc """
  Creates a feedback.

  ## Examples

      iex> create_feedback(%{field: value})
      {:ok, %Feedback{}}

      iex> create_feedback(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_feedback(attrs \\ %{}) do
    %Feedback{}
    |> Feedback.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a feedback.

  ## Examples

      iex> update_feedback(feedback, %{field: new_value})
      {:ok, %Feedback{}}

      iex> update_feedback(feedback, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_feedback(%Feedback{} = feedback, attrs) do
    feedback
    |> Feedback.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Feedback.

  ## Examples

      iex> delete_feedback(feedback)
      {:ok, %Feedback{}}

      iex> delete_feedback(feedback)
      {:error, %Ecto.Changeset{}}

  """
  def delete_feedback(%Feedback{} = feedback) do
    Repo.delete(feedback)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking feedback changes.

  ## Examples

      iex> change_feedback(feedback)
      %Ecto.Changeset{source: %Feedback{}}

  """
  def change_feedback(%Feedback{} = feedback) do
    Feedback.changeset(feedback, %{})
  end
end
