defmodule Ceiba.Core.Helpers do
  def parse_integer(no) do
    if is_integer(no) do
      {:ok, no}
    else
      case Integer.parse(no) do
        {number, _} -> {:ok, number}
        :error -> :error
      end
    end
  end
end
