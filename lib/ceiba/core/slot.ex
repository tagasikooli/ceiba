defmodule Ceiba.Core.Slot do
  use Ecto.Schema
  import Ecto.Changeset

  schema "slots" do
    field :email_sent_booking_closed, :boolean
    field :count_registrations, :integer, virtual: true
    embeds_one :info, Ceiba.Core.SlotInfo
    has_one :registrations, Ceiba.Core.SlotRegistration

    timestamps()
  end

  @doc false
  def changeset(slot, attrs) do
    slot
    |> cast(attrs, [:email_sent_booking_closed])
    |> cast_embed(:info)
    |> validate_required([])
  end
end
