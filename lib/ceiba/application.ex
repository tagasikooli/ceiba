defmodule Ceiba.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    # List all child processes to be supervised
    children = [
      # Start scheduler to send emails
      worker(Ceiba.Scheduler, []),
      # Start the Ecto repository
      Ceiba.Repo,
      # Start the endpoint when the application starts
      CeibaWeb.Endpoint
      # Starts a worker by calling: Ceiba.Worker.start_link(arg)
      # {Ceiba.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ceiba.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CeibaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
