defmodule CeibaWeb.FeedbackController do
  use CeibaWeb, :controller

  plug BasicAuth,
       [use_config: {:ceiba, :basic}] when action in [:index, :show, :edit, :update, :delete]

  alias Ceiba.Core
  alias Ceiba.Core.Feedback

  def index(conn, _params) do
    feedback = Core.list_feedback()
    render(conn, "index.html", feedback: feedback)
  end

  def new(conn, %{"reg_id" => reg_id, "r" => role}) do
    registration = Core.get_slot_registration!(reg_id)
    slot = Core.get_slot!(registration.slot_id)
    changeset = Core.change_feedback(%Feedback{slot_registration_id: reg_id, role: role})

    render(conn, "new.html", %{
      changeset: changeset,
      registration: registration,
      slot: slot,
      role: role
    })
  end

  def create(conn, %{"feedback" => feedback_params}) do
    registration = Core.get_slot_registration!(feedback_params["slot_registration_id"])
    slot = Core.get_slot!(registration.slot_id)

    case Core.create_feedback(feedback_params) do
      {:ok, feedback} ->
        conn
        |> put_flash(:info, "Feedback created successfully.")
        |> redirect(to: Routes.feedback_path(conn, :success, %{}))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", %{changeset: changeset, registration: registration, slot: slot})
    end
  end

  def success(conn, _params) do
    render(conn, "success.html")
  end

  def show(conn, %{"id" => id}) do
    feedback = Core.get_feedback!(id)
    registration = Core.get_slot_registration!(feedback.slot_registration_id)
    slot = Core.get_slot!(registration.slot_id)
    render(conn, "show.html", %{feedback: feedback, slot: slot})
  end

  def update(conn, %{"id" => id, "feedback" => feedback_params}) do
    feedback = Core.get_feedback!(id)

    case Core.update_feedback(feedback, feedback_params) do
      {:ok, feedback} ->
        conn
        |> put_flash(:info, "Feedback updated successfully.")
        |> redirect(to: Routes.feedback_path(conn, :show, feedback))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", feedback: feedback, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    feedback = Core.get_feedback!(id)
    {:ok, _feedback} = Core.delete_feedback(feedback)

    conn
    |> put_flash(:info, "Feedback deleted successfully.")
    |> redirect(to: Routes.feedback_path(conn, :index))
  end
end
