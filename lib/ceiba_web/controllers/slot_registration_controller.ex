defmodule CeibaWeb.SlotRegistrationController do
  use CeibaWeb, :controller

  plug(
    BasicAuth,
    [use_config: {:ceiba, :basic}] when action in [:index, :show, :edit, :update, :delete]
  )

  alias Ceiba.Core
  alias Ceiba.Core.SlotRegistration

  def index(conn, _params) do
    slots_registration = Core.list_slots_registration()
    render(conn, "index.html", slots_registration: slots_registration)
  end

  def new(conn, %{"slot_id" => slot_id}) do
    changeset = Core.change_slot_registration(%SlotRegistration{slot_id: slot_id})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"slot_registration" => slot_registration_params}) do
    case Core.create_slot_registration(slot_registration_params) do
      {:ok, slot_registration} ->
        Ceiba.Email.slot_registration(conn, slot_registration)

        conn
        |> put_flash(:info, "Slot registration created successfully.")
        |> redirect(to: Routes.slot_registration_path(conn, :success, %{}))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def success(conn, _params) do
    render(conn, "success.html")
  end

  def show(conn, %{"id" => id}) do
    slot_registration = Core.get_slot_registration!(id)
    render(conn, "show.html", slot_registration: slot_registration)
  end

  def edit(conn, %{"id" => id}) do
    slot_registration = Core.get_slot_registration!(id)
    changeset = Core.change_slot_registration(slot_registration)
    render(conn, "edit.html", slot_registration: slot_registration, changeset: changeset)
  end

  def update(conn, %{"id" => id, "slot_registration" => slot_registration_params}) do
    slot_registration = Core.get_slot_registration!(id)

    case Core.update_slot_registration(slot_registration, slot_registration_params) do
      {:ok, slot_registration} ->
        conn
        |> put_flash(:info, "Slot registration updated successfully.")
        |> redirect(to: Routes.slot_registration_path(conn, :show, slot_registration))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", slot_registration: slot_registration, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    slot_registration = Core.get_slot_registration!(id)
    {:ok, _slot_registration} = Core.delete_slot_registration(slot_registration)

    conn
    |> put_flash(:info, "Slot registration deleted successfully.")
    |> redirect(to: Routes.slot_registration_path(conn, :index))
  end
end
