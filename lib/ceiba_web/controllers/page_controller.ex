defmodule CeibaWeb.PageController do
  use CeibaWeb, :controller
  alias Ceiba.Core

  def index(conn, params) do
    slots =
      Core.list_slots(params, false)
      |> Enum.sort_by(&Date.to_erl(&1.info.kuupaev))

    render(conn, "index.html", slots: slots)
  end
end
