defmodule CeibaWeb.WidgetController do
  use CeibaWeb, :controller
  alias Ceiba.Core

  plug Ceiba.AllowIframe
  plug :put_layout, "widget.html"

  def index(conn, params) do
    slots = Core.list_slots(params)
    render(conn, "index.html", slots: slots)
  end
end
