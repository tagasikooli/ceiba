defmodule CeibaWeb.SlotController do
  use CeibaWeb, :controller

  plug BasicAuth,
       [use_config: {:ceiba, :basic}]
       when action in [:index, :edit, :update, :delete, :show_detailed]

  alias Ceiba.Core
  alias Ceiba.Core.Slot

  def index(conn, params) do
    slots =
      Core.list_slots_with_feedback(params)
      |> Enum.filter(&(!String.contains?(&1.info.email || "", "krister.ee")))

    fields = CeibaWeb.SlotView.all_data_fields(slots)
    render(conn, "index.html", %{slots: slots, fields: fields})
  end

  def new(conn, _params) do
    changeset = Core.change_slot(%Slot{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"slot" => slot_params}) do
    case Core.create_slot(slot_params) do
      {:ok, slot} ->
        conn
        |> put_flash(:info, "Slot created successfully.")
        |> redirect(to: Routes.slot_path(conn, :show, slot))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    slot = Core.get_slot!(id)
    render(conn, "show.html", slot: slot)
  end

  def show_detailed(conn, %{"id" => id}) do
    slot = Core.get_slot!(id)
    registrations = Core.get_slot_registrations!(id)
    feedback = Core.get_slot_feedback(id)

    render(conn, "show-detailed.html", %{
      slot: slot,
      registrations: registrations,
      feedback: feedback
    })
  end

  def edit(conn, %{"id" => id}) do
    slot = Core.get_slot!(id)
    changeset = Core.change_slot(slot)

    render(conn, "edit.html", slot: slot, changeset: changeset)
  end

  def update(conn, %{"id" => id, "slot" => slot_params}) do
    slot = Core.get_slot!(id)

    case Core.update_slot(slot, slot_params) do
      {:ok, slot} ->
        conn
        |> put_flash(:info, "Slot updated successfully.")
        |> redirect(to: Routes.slot_path(conn, :show, slot))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", slot: slot, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    slot = Core.get_slot!(id)
    {:ok, _slot} = Core.delete_slot(slot)

    conn
    |> put_flash(:info, "Slot deleted successfully.")
    |> redirect(to: Routes.slot_path(conn, :index))
  end
end
