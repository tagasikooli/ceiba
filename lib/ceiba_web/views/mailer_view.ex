defmodule CeibaWeb.EmailView do
  use CeibaWeb, :view

  def slot_fields do
    Application.get_env(:ceiba, Custom)[:event_types][:excursion][:slot_info_fields]
  end

  def registration_fields do
    Application.get_env(:ceiba, Custom)[:event_types][:excursion][:registration_info_fields]
  end
end
