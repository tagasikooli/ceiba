defmodule CeibaWeb.SlotRegistrationView do
  use CeibaWeb, :view

  def extra_fields do
    for embed_field <- Ceiba.Core.SlotRegistration.__schema__(:embeds),
        {:embed, embedded} = Ceiba.Core.SlotRegistration.__schema__(:type, embed_field),
        field <- embedded.related.__schema__(:fields),
        into: %{},
        do: {field, embedded.related.__schema__(:type, field)}
  end

  def config_fields do
    Application.get_env(:ceiba, Custom)[:event_types][:excursion][:registration_info_fields]
  end
end
