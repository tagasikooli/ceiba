defmodule CeibaWeb.SlotView do
  use CeibaWeb, :view
  alias Ceiba.Core

  def extra_fields do
    for embed_field <- Ceiba.Core.Slot.__schema__(:embeds),
        {:embed, embedded} = Ceiba.Core.Slot.__schema__(:type, embed_field),
        field <- embedded.related.__schema__(:fields),
        into: %{},
        do: {field, embedded.related.__schema__(:type, field)}
  end

  def count_registrations(slot_id) do
    Core.count_slots_registration(slot_id)
  end

  def config_fields do
    Application.get_env(:ceiba, Custom)[:event_types][:excursion][:slot_info_fields]
  end

  def conf_fields_feedback(role) do
    Application.get_env(:ceiba, Custom)[:feedback_questions][role]
  end

  def all_data_fields(slots) do
    default =
      case Enum.fetch(slots, 0) do
        {:ok, default} -> default
        :error -> nil
      end

    if default == nil do
      []
    else
      case Enum.find(slots, default, fn x -> x.feedback["teacher"] end) do
        slot ->
          feedback_teacher =
            Map.new(slot.feedback["teacher"] || %{}, fn {key, val} ->
              {key, val}
            end)

          feedback_student =
            Map.new(slot.feedback["student"] || %{}, fn {key, val} ->
              {key, val}
            end)

          slot.info
          |> Map.merge(feedback_teacher)
          |> Map.merge(feedback_student)
          |> Map.to_list()
      end
    end
  end

  def get_single_slot(slots) do
    default =
      case Enum.fetch(slots, 0) do
        {:ok, default} -> default
        :error -> nil
      end

    if default == nil do
      []
    else
      case Enum.find(slots, default, fn x -> x.feedback["teacher"] end) do
        slot -> slot
      end
    end
  end

  def get_status(slot) do
    cond do
      slot.count_registrations > 0 && Date.compare(slot.info.kuupaev, Date.utc_today()) == :lt ->
        "Läbitud"

      slot.count_registrations > 0 ->
        "Broneeritud"

      Date.compare(slot.info.reg_sulgemine, Date.utc_today()) == :lt ->
        "Aegunud"

      true ->
        "Uus"
    end
  end

  def count_statuses(slots) do
    Enum.reduce(slots, %{}, fn slot, acc -> Map.update(acc, get_status(slot), 1, &(&1 + 1)) end)
  end
end
