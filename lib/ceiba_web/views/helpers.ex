defmodule CeibaWeb.ViewHelpers do
  use CeibaWeb, :view

  def key_to_question(list_of_maps, key, str_supplement \\ "") do
    key =
      if is_bitstring(key) do
        key
      else
        Atom.to_string(key)
      end

    list_of_maps
    |> Enum.find(&(Atom.to_string(&1.key) == key))
    |> (fn details -> get_label(details, str_supplement) end).()
  end

  def get_label(details, str_supplement) do
    if Map.has_key?(details, :label_fn) do
      details.label_fn.(str_supplement)
    else
      details.label
    end
  end
end
