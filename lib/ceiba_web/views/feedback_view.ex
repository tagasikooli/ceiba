defmodule CeibaWeb.FeedbackView do
  use CeibaWeb, :view

  def config_fields(role) do
    Application.get_env(:ceiba, Custom)[:feedback_questions][role]
  end
end
