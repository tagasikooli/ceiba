defmodule CeibaWeb.Router do
  use CeibaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CeibaWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/widget", WidgetController, :index
    get "/slot-detailed/:id", SlotController, :show_detailed
    resources "/slot", SlotController
    get "/slot-registration/success", SlotRegistrationController, :success
    resources "/slot-registration", SlotRegistrationController
    get "/feedback/success", FeedbackController, :success
    resources "/feedback", FeedbackController
  end

  # Other scopes may use custom stacks.
  # scope "/api", CeibaWeb do
  #   pipe_through :api
  # end
end
