# https://github.com/bitwalker/alpine-elixir-phoenix
FROM bitwalker/alpine-elixir-phoenix:latest

# Cache elixir deps
ADD mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

# Cache npm deps and static files
ADD assets/ assets/
RUN cd assets && npm install && node node_modules/webpack/bin/webpack.js --mode development

# Add rest of the files
Add . .

# USER default

CMD ["mix", "do", "ecto.migrate,", "phx.server"]
